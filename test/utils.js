import {prisma} from "../src/application/database.js";
import bcrypt from "bcrypt";

const deleteUserTest = async () => {
    await prisma.user.deleteMany({
            where :{
                username : "heru"
            }
        })
}
const createUserTest = async () => {
    await prisma.user.create({
            data : {
                username : "heru",
                password : await bcrypt.hash("rahasia",10),
                name : "Heru",
                token: "test-token"
            }
        })
}
const deleteGroupTest = async () => {
    prisma.group.deleteMany({
            where : {
                group_name : 'premium'
            }
        })
}
export default {
    createUserTest,
    deleteGroupTest,
    deleteUserTest
}
