import {prisma} from "../src/application/database.js";
import supertest from "supertest";
import {web} from "../src/application/web.js";
import utils from "./utils.js";
describe("POST /api/v1/groups", function () {
    afterEach(async () => {
        await utils.deleteGroupTest();
        await utils.deleteUserTest();
    })
    beforeEach(async () => {
        await utils.createUserTest();
    })
    it('should can create group',async function () {
        const result = await supertest(web)
            .post('/api/v1/groups')
            .set("Authorization",'test-token')
            .send({
                group_name : "premium"
            });
        expect(result.status).toBe(201);
        expect(result.body.data.group_name).toBe("premium");
    });
})