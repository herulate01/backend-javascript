import supertest from "supertest";
import {web} from "../src/application/web.js";
import {prisma} from "../src/application/database.js";
import utils from "./utils.js";

describe("POST /api/v1/users/register", function () {
    afterEach(async () => {
        await utils.deleteUserTest()
    });

    it('should can register users', async  () =>  {
        const result = await supertest(web)
            .post('/api/v1/users/register')
            .send({
                username: "heru",
                password: "rahasia",
                confirmPassword: "rahasia",
                name : "Heru"
            });
        expect(result.status).toBe(201);
    });

    it('should cant register users, password confirmation not match', async  () =>  {
        const result = await supertest(web)
            .post('/api/v1/users/register')
            .send({
                username: "heru",
                password: "rahasia",
                confirmPassword: "rahasia2",
                name : "Heru"
            });
        expect(result.status).toBe(400);
    });
    it('should cant register users, invalid request', async  () =>  {
        const result = await supertest(web)
            .post('/api/v1/users/register')
            .send({
                username: "heru",
                password: "rahasia",
                confirmPassword: "rahasia",
                name : ""
            });
        expect(result.status).toBe(400);
    });
});

describe("POST /api/v1/users/login", function () {
    beforeEach(async() => {
        await  utils.createUserTest();
    })
    afterEach(async () => {
        await utils.deleteUserTest();
    });

    it('should can login users', async () =>  {
        const result = await supertest(web)
            .post('/api/v1/users/login')
            .send({
                username: "heru",
                password: "rahasia"
            });
        expect(result.status).toBe(200);
    });
    it('should cant login users, username and password wrong', async  () =>  {
        const result = await supertest(web)
            .post('/api/v1/users/login')
            .send({
                username: "herua",
                password: "adfaads"
            });
        expect(result.status).toBe(401);
    });
});

describe("GET /api/v1/users/", () => {
   beforeEach(async () => {
       await utils.createUserTest();
   });
    afterEach(async () => {
        await utils.deleteUserTest();
    });
    it('should can get data user', async function () {
        const result = await supertest(web)
            .get('/api/v1/users')
            .set('Authorization', 'test-token')
        expect(result.status).toBe(200);
    });

    it('should cant get data user, unauthorized', async function () {
        const result = await supertest(web)
            .get('/api/v1/users')
            .set('Authorization', 'test-toke')
        expect(result.status).toBe(401);
    });
});

describe("PATCH /api/v1/users/change-password", () => {
    beforeEach(async () => {
        await utils.createUserTest();
    });
    afterEach(async () => {
        await utils.deleteUserTest();
    });
    it('should can change password',async function () {
        const result = await supertest(web)
            .patch('/api/v1/users/change-password')
            .set("Authorization",'test-token')
            .send({
                oldPassword : "rahasia",
                newPassword : "secret-password",
                confirmPassword : "secret-password"
            })
        expect(result.status).toBe(200);
    });
    it('should cant change password, old password is not valid', async function () {
        const result = await supertest(web)
            .patch('/api/v1/users/change-password')
            .set("Authorization",'test-token')
            .send({
                oldPassword : "rahasia-not-valid",
                newPassword : "secret-password",
                confirmPassword : "secret-password"
            })
        expect(result.status).toBe(401);
    });
    it('should cant change password, password confirmation not match', async function () {
        const result = await supertest(web)
            .patch('/api/v1/users/change-password')
            .set("Authorization",'test-token')
            .send({
                oldPassword : "rahasia",
                newPassword : "secret-password",
                confirmPassword : "secret-password2"
            })
        expect(result.status).toBe(400);
    });
});
describe('PATCH /api/v1/users', function () {
    beforeEach(async () => {
        await utils.createUserTest();
    });
    afterEach(async () => {
        await prisma.user.deleteMany({
            where :{
                username : "herulate"
            }
        })
    });
    it('should can update user', async function () {
        const result = await supertest(web)
            .patch('/api/v1/users')
            .set("Authorization",'test-token')
            .send({
                username : 'herulate',
                name : "Heru Late"

            })
        expect(result.status).toBe(200);
    });
    it('should cant update user, invalid request', async function () {
        const result = await supertest(web)
            .patch('/api/v1/users')
            .set("Authorization",'test-token')
            .send({
                username : '',
                name : "Heru Late"

            })
        expect(result.status).toBe(400);
    });
    it('should cant update user, unauthorized', async function () {
        const result = await supertest(web)
            .patch('/api/v1/users')
            .set("Authorization",'test-token2')
            .send({
                username : 'herulate',
                name : "Heru Late"

            })
        expect(result.status).toBe(401);
    });
});
describe('DELETE /api/v1/users/logout', function () {
    beforeEach(async () => {
        await utils.createUserTest();
    });
    afterEach(async () => {
        await utils.deleteUserTest();
    });
    it('should can logout user',async function () {
        const result = await supertest(web)
            .delete('/api/v1/users/logout')
            .set("Authorization",'test-token')
        expect(result.status).toBe(200);
        expect(result.body.data).toBe("OK");
    });
});