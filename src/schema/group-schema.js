import Joi from "joi";

const createGroupSchema = Joi.object({
    group_name : Joi.string().required()
});
const getGroupSchema = Joi.string().required();

const updateGroupSchema = Joi.object({
    id : Joi.string().required(),
    group_name : Joi.string().required()
});

export {
    createGroupSchema,
    getGroupSchema,
    updateGroupSchema
}