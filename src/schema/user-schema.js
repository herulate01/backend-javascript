import Joi from "joi";
const userRegistrationSchema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().min(6).required(),
    confirmPassword: Joi.string().valid(Joi.ref('password')).required(),
    name : Joi.string().required()
});
const userLoginSchema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().min(6).required(),
});
const getUserSchema = Joi.string().required();
const userUpdateSchema = Joi.object({
    id : Joi.string().required(),
    username: Joi.string().optional(),
    name : Joi.string().optional(),
    status : Joi.number().optional()
});
const userUpdatePassword = Joi.object({
    id : Joi.string().required(),
    oldPassword : Joi.string().min(6).required(),
    newPassword : Joi.string().min(6).required(),
    confirmPassword : Joi.string().valid(Joi.ref("newPassword")).required()
});


export  {
    userRegistrationSchema,
    userLoginSchema,
    getUserSchema,
    userUpdatePassword,
    userUpdateSchema
}

