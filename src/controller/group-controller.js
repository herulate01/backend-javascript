import groupRepository from "../repository/group-repository.js";

const createGroup = async (req, res, next) => {
    try {
        const result = await groupRepository.createGroup(req.body);
        res.status(201).json({
            data : result
        })
    }catch (e) {
        next(e);
    }
}
const getGroup = async (req, res, next) => {
    try {
        const groupId = req.params.groupId;
        const result = await groupRepository.getGroup(groupId);
        res.status(200).json({
            data : result
        })
    }catch (e) {
        next(e);
    }
}
const updateGroup = async (req, res, next) => {
    try {
        const groupId = req.params.groupId;
        const request = req.body;
        request.id = groupId;
        const result = await groupRepository.updateGroup(request);
        res.status(200).json({
            data : result
        })
    }catch (e) {
        next(e);
    }
}

const deleteGroup = async (req, res, next) => {
    try {
        const groupId = req.params.groupId;
        const result = await groupRepository.deleteGroup(groupId);
        res.status(200).json({
            data : "OK"
        })
    }catch (e) {
        next(e);
    }
}
export  default {
    createGroup,
    getGroup,
    updateGroup,
    deleteGroup
}