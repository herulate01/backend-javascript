import userRepository from "../repository/user-repository.js";
import {logger} from "../application/log.js";

const register = async (req, res, next) => {
    try {
        const result = await userRepository.register(req.body);
        res.status(201).json({
            data : result
        });
    }catch (e) {
        logger.error(e.message);
        next(e);
    }
}
const login = async (req, res, next) => {
    try{
        const result = await userRepository.login(req.body);
        res.status(200).json({
            data : result
        })
    }catch (e) {
        logger.error(e.message);
        next(e);
    }
}
const get = async (req, res, next) => {
    try {
        const userid = req.user.id;
        const result = await userRepository.getUser(userid);
        res.status(200).json({
            data : result
        });
    }catch (e) {
        logger.error(e.message);
        next(e)
    }
}

const changePassword = async (req,res,next) => {
    try {
        const userid = req.user.id;
        const request = req.body;
        request.id = userid;
        const result = await userRepository.changePassword(request);
        res.status(200).json({
            data : result
        });
    }catch (e) {
        logger.error(e.message);
        next(e);
    }
}

const update = async (req, res, next) => {
    try {
        const userid = req.user.id;
        const request = req.body;
        request.id = userid;
        const result = await userRepository.update(request);
        res.status(200).json({
            data :result
        });
    }catch (e){
        logger.error(e.message);
        next(e);
    }
}

const logout = async (req, res, next) => {
    try {
        const userid = req.user.id;
        const result = await userRepository.logout(userid);
        res.status(200).json({
            data : "OK"
        });
    }catch (e) {
        next(e);
    }
}
export default {
    register,
    login,
    get,
    changePassword,
    update,
    logout
}