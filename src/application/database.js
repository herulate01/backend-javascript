import {PrismaClient} from "@prisma/client";
import {logger} from "./log.js";

const prisma = new PrismaClient({
    log :[
        {
            emit: 'event',
            level: 'query',
        },
        {
            emit: 'event',
            level: 'error',
        },
        {
            emit: 'event',
            level: 'info',
        },
        {
            emit: 'event',
            level: 'warn',
        },
    ]
});
prisma.$on('query', (event) =>{
    logger.info(event);
})
prisma.$on('error', (error) => {
    logger.error(error);
})
prisma.$on('info', (event) => {
    logger.info(event);
})
prisma.$on('warn', (event) => {
    logger.warn(event);
})

export {
    prisma
}