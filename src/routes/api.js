import express from "express";
import userController from "../controller/user-controller.js";
import {authMiddleware} from "../middleware/auth-middleware.js";
import groupController from "../controller/group-controller.js";

const api = express.Router();
api.use(authMiddleware);

api.get('/api/v1/users', userController.get);
api.patch('/api/v1/users/change-password', userController.changePassword);
api.patch('/api/v1/users', userController.update);
api.delete('/api/v1/users/logout', userController.logout)

/* API Group */
api.post('/api/v1/groups', groupController.createGroup);
api.get('/api/v1/groups/:groupId', groupController.getGroup);
api.put('/api/v1/groups/:groupId', groupController.updateGroup);
api.delete('/api/v1/groups/:groupId', groupController.deleteGroup);
export {
    api
}

