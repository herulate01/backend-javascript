import express from "express";
import userController from "../controller/user-controller.js";

const publicApi = express.Router();

publicApi.post('/api/v1/users/register', userController.register);
publicApi.post('/api/v1/users/login', userController.login);

export  {
    publicApi
}