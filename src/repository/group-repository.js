import {validate} from "../application/validation.js";
import {createGroupSchema, getGroupSchema, updateGroupSchema} from "../schema/group-schema.js";
import {prisma} from "../application/database.js";
import {ResponseError} from "../application/error.js";

const createGroup = async (request) => {
    const group = validate(createGroupSchema, request);
    const groupExist = await prisma.group.count({
        where : {
            group_name : group.group_name
        }
    });
    if(groupExist === 1) {
        throw new ResponseError(409, "This group name is already created");
    }
    return prisma.group.create({
        data : group,
        select : {
            group_name : true
        }
    })
}
const getGroup = async (groupId) => {
    groupId = validate(getGroupSchema, groupId);
    const group = await prisma.group.findUnique({
        where : {
            id : groupId
        },
        select : {
            group_name : true
        }
    });
    if(!group) {
        throw new ResponseError(404, "group is not found");
    }
    return group;
}
const updateGroup = async (request) => {
    const group = validate(updateGroupSchema, request);
    const groupExist = await prisma.group.findUnique({
        where : {
            id :group.id
        }
    });
    if(!groupExist) {
        throw new ResponseError(404, "group is not found");
    }
    const data = {};
    if(group.group_name){
        const groupNameExist = await prisma.group.findFirst({
            where : {
                group_name : group.group_name
            }
        })
        if(groupNameExist) {
            throw new ResponseError(409, "group name is already exists");
        }
        data.group_name = group.group_name;
    }
    return prisma.group.update({
        where : {
            id : group.id
        },
        data : data,
        select : {
            group_name : true
        }
    });
}

const deleteGroup = async (groupId) => {
    groupId = validate(getGroupSchema, groupId);
    const group = await prisma.group.findUnique({
        where : {
            id : groupId
        },
        select : {
            group_name : true
        }
    });
    if(!group) {
        throw new ResponseError(404, "group is not found");
    }
    return prisma.group.delete({
        where : {
            id : groupId
        }
    });
}

export default {
    createGroup,
    getGroup,
    updateGroup,
    deleteGroup
}