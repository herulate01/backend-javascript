import {validate} from "../application/validation.js";
import {
    getUserSchema,
    userLoginSchema,
    userRegistrationSchema,
    userUpdatePassword,
    userUpdateSchema
} from "../schema/user-schema.js";
import {prisma} from "../application/database.js";
import {ResponseError} from "../application/error.js";
import bcrypt from "bcrypt";
import {v4 as uuid} from "uuid";
const register = async (request) => {
    const user = validate(userRegistrationSchema, request);
    const userExist = await prisma.user.count({
        where : {
            username : user.username
        }
    });
    if(userExist === 1) {
        throw new ResponseError(409, "username has already registered")
    }
    user.password = await bcrypt.hash(user.password, 10);
    return prisma.user.create({
        data : {
            username : user.username,
            password : user.password,
            name : user.name
        },
        select:{
            username : true,
            name : true
        }
    })
}

const login = async (request) =>{
    const user = validate(userLoginSchema, request);
    const userExist = await prisma.user.findFirst({
        where : {
            username : user.username
        },
        select : {
            id : true,
            password : true,
            username : true
        }
    });
    if(!userExist) {
        throw new ResponseError(401, "username and password wrong")
    }
    const isValidPassword = await bcrypt.compare(user.password, userExist.password);

    if(!isValidPassword) {
        throw new ResponseError(401, "username and password wrong");
    }
    const token = uuid().toString();
    return prisma.user.update({
        where : {
            id : userExist.id
        },
        data :{
            token : token
        },
        select : {
            token:true
        }
    })
}

const getUser = async (userId) => {
    userId = validate(getUserSchema, userId);
    const userExist = await prisma.user.findUnique({
        where : {
            id :userId
        },
        select : {
            username :true,
            name : true,
        }
    });
    if(!userExist){
        throw new ResponseError(404, "user is not found");
    }
    return userExist;
}

const changePassword = async (request) => {
    const user = validate(userUpdatePassword, request);
    const userExist = await prisma.user.findUnique({
        where : {
            id : user.id
        },
        select:{
            id : true,
            password : true,
        }
    });
    if(!user) {
        throw new ResponseError(404, "user is not found");
    }
    const isValidPass = await bcrypt.compare(user.oldPassword, userExist.password);
    if(!isValidPass) {
        throw new ResponseError(401, "Your password is not valid");
    }
    return prisma.user.update({
        data : {
            password : await bcrypt.hash(user.newPassword, 10)
        },
        where : {
            id : userExist.id
        }
    });
}

const update = async (request) => {
    const user = validate(userUpdateSchema, request);
    const userExist = await prisma.user.findUnique({
        where : {
            id : user.id
        }
    });
    if(!userExist) {
        throw new ResponseError(404, "user is not found");
    }
    const data = {};
    if(user.username) {
        const validateUsername = await prisma.user.findFirst({
            where : {
                username : user.username
            }
        });
        if(validateUsername) {
            throw new ResponseError(409, "username is already exists, ")
        }
        data.username  = user.username;
    }
    if(user.name) {
        data.name = user.name;
    }
    return prisma.user.update({
        data : data,
        where :{
            id : user.id
        },
        select : {
            username : true,
            name : true
        }
    })
}
const logout = async (userid) => {
    userid = validate(getUserSchema, userid);
    const user = await prisma.user.findUnique({
        where : {
            id : userid
        },
        select: {
            id:true
        }
    });
    if(!user) {
        throw new ResponseError(404, "user is not found");
    }
    return prisma.user.update({
        data : {
            token : null
        },
        where : {
            id : user.id
        }
    });
}

export default {
    register,
    login,
    getUser,
    changePassword,
    update,
    logout
}