/*
  Warnings:

  - You are about to drop the `UserToGroup` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `UserToGroup` DROP FOREIGN KEY `UserToGroup_groupId_fkey`;

-- DropForeignKey
ALTER TABLE `UserToGroup` DROP FOREIGN KEY `UserToGroup_userId_fkey`;

-- DropTable
DROP TABLE `UserToGroup`;

-- CreateTable
CREATE TABLE `users_to_groups` (
    `userId` VARCHAR(191) NOT NULL,
    `groupId` VARCHAR(191) NOT NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`userId`, `groupId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `users_to_groups` ADD CONSTRAINT `users_to_groups_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `users_to_groups` ADD CONSTRAINT `users_to_groups_groupId_fkey` FOREIGN KEY (`groupId`) REFERENCES `groups`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
